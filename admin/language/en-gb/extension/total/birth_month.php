<?php
// Heading
$_['heading_title']    = 'Birth Month Discount';

// Text
$_['text_success']     = 'Success: You have modified Birth Month Discount total!';
$_['text_edit']        = 'Edit Birth Month Discount';

// Entry
$_['entry_status']     = 'Status';
$_['entry_discount_rate']     = 'Discount rate (%)';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Birth Month Discount!';