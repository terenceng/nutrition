<?php
    class ControllerExtensionModuleTeamStaff extends Controller {
	private $error = array();

	public function index() {

        $choices = array(
            array(
                'value' => "Ambassador",
                'label' => "Ambassador"
            ),
            array(
                'value' => "Athlete",
                'label' => "Athlete",
            )
        );
            
        $array = array(
            'oc' => $this,
            'heading_title' => 'Athlete / Ambassador',
            'modulename' => 'team_staff',
                'fields' => array(
                    array('type' => 'repeater', 'label' => 'Places', 'name' => 'places', 'fields' => array(
                        array('type' => 'text', 'label' => 'Title', 'name' => 'title'),
                        array('type' => 'image', 'label' => 'Image (355px x 537px)', 'name' => 'image'),
                        array('type' => 'dropdown', 'label' => 'Position', 'name' => 'position', 'choices' => $choices),
                        array('type' => 'textarea', 'label' => 'Description', 'name' => 'description')
                    ),
                ), // end of repeater
            ),
        );
        $this->modulehelper->init($array);    
  }

}