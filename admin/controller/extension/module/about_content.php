<?php
class ControllerExtensionModuleAboutcontent extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'About Page Content',
      'modulename' => 'about_content',
      'fields' => array(
        array('type' => 'text', 'label' => 'Top Section Main Title', 'name' => 'about_top_main_title'),
        array('type' => 'image', 'label' => 'Top right image', 'name' => 'about_top_image'),
        array('type' => 'textarea', 'label' => 'Top Content Text', 'name' => 'about_top_content_text'),

        array('type' => 'text', 'label' => '2nd Section Left Title', 'name' => 'section2_left_title'),
        array('type' => 'textarea', 'label' => '2nd Section Left Text', 'name' => 'section2_left_text'),
        array('type' => 'text', 'label' => '2nd Section Right Title', 'name' => 'section2_right_title'),
        array('type' => 'textarea', 'label' => '2nd Section Right Text', 'name' => 'section2_right_text'),


        array('type' => 'image', 'label' => '3rd Section image', 'name' => 'section3_image'),
        array('type' => 'textarea', 'label' => '3rd Section Text', 'name' => 'section3_text'),
        array('type' => 'repeater', 'label' => '3rd Section logos', 'name' => 'logos',
          'fields' => array(
            array ('type' => 'image', 'label' => '3rd section Logo', 'name' => 'logo'),
          )
        ),

        array('type' => 'text', 'label' => 'Section 4 Title', 'name' => 'section4_title'),
        array('type' => 'textarea', 'label' => 'Section 4 Text', 'name' => 'section4_text'),
        array('type' => 'image', 'label' => 'Section 4 Image', 'name' => 'section4_image'),


        array('type' => 'text', 'label' => 'Section 5 Title', 'name' => 'section5_title'),
        array('type' => 'textarea', 'label' => 'Section 5 Text', 'name' => 'section5_text'),
        array('type' => 'image', 'label' => 'Section 5 Image', 'name' => 'section5_image'),

        
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
