<?php
class ControllerExtensionModuleHomeAboutcontent extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'Home About Page Content',
      'modulename' => 'home_about_content',
      'fields' => array(

        array('type' => 'image', 'label' => 'Home About Image', 'name' => 'left_about_image'),
        array('type' => 'text', 'label' => 'SubHeading', 'name' => 'subheading'),
        array('type' => 'textarea', 'label' => 'For Video Use', 'name' => 'video'),
        array('type' => 'textarea', 'label' => 'Home About Text', 'name' => 'right_about_text'),
        
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
