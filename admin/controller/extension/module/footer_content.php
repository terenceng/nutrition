<?php
class ControllerExtensionModuleFootercontent extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'Footer Content',
      'modulename' => 'footer_content',
      'fields' => array(

        array('type' => 'repeater', 'label' => 'Store Location', 'name' => 'store_location',
          'fields' => array(
            array ('type' => 'text', 'label' => 'Title/Store name', 'name' => 'store_name'),
            array ('type' => 'text', 'label' => 'Extra Text', 'name' => 'extra_text'),
            array ('type' => 'textarea', 'label' => 'Address', 'name' => 'address'),
            array ('type' => 'image', 'label' => 'Store Image', 'name' => 'store_image'),
          )
        ),

        array('type' => 'textarea', 'label' => 'Contact Detail text', 'name' => 'contact_detail_text'),
        
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
