<?php
class ControllerExtensionModuleHomeSecondaryBanners extends Controller {
  private $error = array();

  public function index() {

    $array = array(
      'oc' => $this,
      'heading_title' => 'Home Secondary Banners Content',
      'modulename' => 'home_secondary_banners',
      'fields' => array(

        array('type' => 'repeater', 'label' => 'Secondary Banners', 'name' => 'banners',
          'fields' => array(
            array ('type' => 'text', 'label' => 'Image Link', 'name' => 'image_link'),
            array ('type' => 'image', 'label' => 'Banner Image', 'name' => 'image'),
          )
        ),
        
      )
    );
    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
    $Modulehelper->init ($array) ;
  }
}
