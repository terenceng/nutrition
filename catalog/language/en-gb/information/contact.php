<?php

$_ = array(
// Heading
    'heading_title'  => 'Contact Us',

// Text
    'text_location'  => 'Our Location',
    'text_store'     => 'Our Stores',
    'text_contact'   => 'Contact Form',
    'text_address'   => 'Address',
    'text_telephone' => 'Telephone',
    'text_fax'       => 'Fax',
    'text_open'      => 'Opening Times',
    'text_comment'   => 'Comments',
    'text_success'   => '<p>Your enquiry has been successfully sent to the store owner!</p>',

// Entry
    'entry_name'        => 'Name',
    'entry_subject'     => 'Subject',
    'entry_email'       => 'E-Mail',
    'entry_telephone'   => 'Telephone',
    'entry_enquiry'     => 'Message',

// Email
    'email_subject'  => 'Enquiry %s',

// Errors
    'error_name'        => 'Name must be between 3 and 32 characters!',
    'error_subject'     => 'Subject must be between 3 and 32 characters!',
    'error_telephone'   => 'Telephone must be all numbers',
    'error_email'       => 'E-Mail Address does not appear to be valid!',
    'error_enquiry'     => 'Enquiry must be between 10 and 3000 characters!',
);
