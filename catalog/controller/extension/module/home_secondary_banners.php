<?php
class ControllerExtensionModuleHomeSecondaryBanners extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'home_secondary_banners';

		$this->document->addStyle('catalog/view/javascript/slick/slick.min.css');
		$this->document->addScript('catalog/view/javascript/slick/slick-custom.min.js');

    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'banners'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'banners'),
		);

		return $this->load->view('extension/module/home_secondary_banners', $data);
	}
}
