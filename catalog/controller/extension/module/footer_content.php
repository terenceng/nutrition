<?php
class ControllerExtensionModuleFootercontent extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'footer_content';

    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'store_location'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'store_location'),
			'contact_detail_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'contact_detail_text'),
		);

		return $data;
	}
}
