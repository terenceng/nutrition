<?php
class ControllerExtensionModuleAboutcontent extends Controller {
	public function index($setting) {
		// if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
		// 	$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
		// 	$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		//
		// 	return $this->load->view('extension/module/html', $data);
		// }
		$oc = $this;
		$language_id = $this->config->get('config_language_id');
		$modulename  = 'about_content';

    $this->load->library('modulehelper');
    $Modulehelper = Modulehelper::get_instance($this->registry);
		$data = array(
			'about_top_main_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_top_main_title'),
			'about_top_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_top_image'),
			'about_top_content_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'about_top_content_text'),

			'section2_left_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section2_left_title'),
			'section2_left_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section2_left_text'),
			'section2_right_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section2_right_title'),
			'section2_right_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section2_right_text'),

			'section3_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section3_image'),
			'section3_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section3_text'),
			'logos'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'logos'),

			'section4_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section4_title'),
			'section4_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section4_text'),
			'section4_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section4_image'),

			'section5_title'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section5_title'),
			'section5_text'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section5_text'),
			'section5_image'  	   => $Modulehelper->get_field ( $oc, $modulename, $language_id, 'section5_image'),
		);

		return $this->load->view('extension/module/about_content', $data);
	}
}
