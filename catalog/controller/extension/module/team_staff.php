<?php
class ControllerExtensionModuleTeamStaff extends Controller {
    public function index($setting) {
        $this->load->library('modulehelper');
        $Modulehelper = Modulehelper::get_instance($this->registry);

        $oc = $this;
        $language_id = $this->config->get('config_language_id');
        $this->load->language('extension/module/team_staff');
        $data['heading_title'] = $this->language->get('heading_title');
        
        $this->document->addStyle('catalog/view/theme/default/stylesheet/magnific-popup.css');
        $this->document->addScript('catalog/view/javascript/jquery.magnific-popup.js');
        
        $modulename  = 'team_staff';
        $data['teams'] = $Modulehelper->get_field ( $oc, $modulename, $language_id, 'places');

        foreach($data['teams'] as $i => $team) {
            $data['teams'][$i]['short_description'] = utf8_substr(strip_tags(html_entity_decode($team['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..';
        }


        return $this->load->view('extension/module/team_staff', $data);
    }
}