<div id="footer-area">
	
<?php if($mailchimp){ ?>
	<div class="newsletter-section text-center">
		<?= $mailchimp; ?>
	</div>
<?php } ?>

<footer>
	<div class="container">
		<div class="row">
			<div class="footer-contact-info">
					<?php if ($footer_content) { ?>
						<div class="col-sm-7">
								<div class="title bold-text">
									<h4>Outlet Locations</h4>
								</div>
							<?php foreach ($footer_content['store_location'] as $index => $location) { ?>
								<?php if( $index < 2) ?> 
								<div class="footer-location">
									<div class="body" id="location<?= $location['location_id']; ?>" >
										<div class="panel-body">
											<div class="row">
												<?php if ($location['store_image']) { ?>
													<div class=""><img src="image/<?= $location['store_image']; ?>" class="img-responsive" alt="<?= $location['store_name']; ?>" title="<?= $location['store_name']; ?>" /></div>
												<?php } ?>
												<br>
												<div class=""><strong><?= $location['store_name']; ?></strong><br />
													<?php if ($location['extra_text']) { ?>
														<div class=""><?= $location['extra_text']; ?></div>
													<?php } ?>
													<address>
														<?= html($location['address']); ?>
													</address>
												</div>
												
												<!-- <div class="col-sm-12">
													<?php if ($location['open']) { ?>
														<?= $location['open']; ?><br />
														<br />
													<?php } ?>
													<?php if ($location['comment']) { ?>
														<?= $location['comment']; ?>
													<?php } ?>
												</div> -->
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>

						<div class="col-sm-5">
							<div class="title bold-text">
								<h4>Contact Details</h4>
							</div>
							<div>
								<?= html($footer_content['contact_detail_text']);?>
							</div>
								
						</div>
					<?php } ?>

			</div>
			<div>
				
			</div>

			
			<!-- 

			<?php if ($menu) { ?>
				<?php foreach($menu as $links){ ?>
				<div class="footer-contact-links">
					<h5>
						<?php if($links['href'] != '#'){ ?>
						<?= $links['name']; ?>
						<?php }else{ ?>
						<a href="<?= $links['href']; ?>" 
							<?php if($links['new_tab']){ ?>
								target="_blank"
							<?php } ?>
							>
							<?= $links['name']; ?></a>
						<?php } ?>
					</h5>
					<?php if($links['child']){ ?>
					<ul class="list-unstyled">
						<?php foreach ($links['child'] as $each) { ?>
						<li><a href="<?= $each['href']; ?>"
							<?php if($each['new_tab']){ ?>
								target="_blank"
							<?php } ?>
							
							>
								<?= $each['name']; ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
				<?php } ?>
			<?php } ?> -->

		</div>

		<div class="footer-bottom"> 
			<div class="row flex align-center">
				<div class="col-xs-12 col-sm-3">
					<?php if($social_icons){ ?>
						<div class="footer-social-icons">
							<?php foreach($social_icons as $icon){ ?>
							<a href="<?= $icon['link']; ?>" title="<?= $icon['title']; ?>" alt="
										<?= $icon['title']; ?>" target="_blank">
								<img src="<?= $icon['icon']; ?>" title="<?= $icon['title']; ?>" class="img-responsive" alt="<?= $icon['title']; ?>" />
							</a>
							<?php } ?>
						</div>
					<?php } ?>
				</div>

				<div class="col-xs-12 col-sm-6 text-center">
					<?= $powered; ?>
				</div>

				<div class="col-xs-12 col-sm-3 text-sm-right">
					<?= $text_fcs; ?>
				</div>
			</div>
		</div>
	</div>
</footer>
</div>
<div id="ToTopHover" ></div>
<script>AOS.init({
	once: true
});</script>
</body></html>