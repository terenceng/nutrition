<div class="search-custom">
    <button class="hidden-xs" type="button" style="background:transparent;border:0;"><img src="image/catalog/search.png" class="pointer"></button>
      <div class="search-box visible-xs">
        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control" />
        <button type="button"><i class="fa fa-search"></i></button>
      </div>
</div>