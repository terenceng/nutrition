<h3 class="product-title bold-text skew_border_1e1e1e_gray"><?= $product_name; ?></h3>
<!-- <ul class="list-unstyled details">
  <?php if ($manufacturer) { ?>
  <li><?= $text_manufacturer; ?> <a href="<?= $manufacturers; ?>"><?= $manufacturer; ?></a></li>
  <?php } ?>
  <li><?= $text_model; ?> <?= $model; ?></li>
  <?php if ($reward) { ?>
  <li><?= $text_reward; ?> <?= $reward; ?></li>
  <?php } ?>
  <li><?= $text_stock; ?> <?= $stock; ?></li>
</ul> -->
<?php if ($price && !$enquiry) { ?>
<ul class="list-unstyled price">
  <?php if (!$special) { ?>
  <li class="price-wrapper">
    <div class="product-price"><?= $price; ?> <div class="inclusive-text"><h4>(Inclusive of GST)</h4></div></div>
  </li>
  <?php } else { ?>
  <li><div class="price-old" style="text-decoration: line-through;"><?= $price; ?></div></li>
  <li class="price-wrapper">
    <div class="price-new product-special-price"><?= $special; ?></div>
    <div class="inclusive-text"><h4>(Inclusive of GST)</h4></div>
  </li>
  <?php } ?>
  <!-- <?php if ($tax) { ?>
  <li class="product-tax-price" ><?= $text_tax; ?> <?= $tax; ?></li>
  <?php } ?> -->
  <?php if ($points) { ?>
  <li><?= $text_points; ?> <?= $points; ?></li>
  <?php } ?>
  <?php if ($discounts) { ?>
  <li>
    <hr>
  </li>
  <?php foreach ($discounts as $discount) { ?>
  <li><?= $discount['quantity']; ?><?= $text_discount; ?><?= $discount['price']; ?></li>
  <?php } ?>
  <?php } ?>
</ul>
<?php } ?>

<?php if($enquiry){ ?>
<div class="enquiry-block">
  <div class="label label-primary">
    <?= $text_enquiry_item; ?>
  </div>
</div>
<?php } ?>
<div class="product-description">
  <?= $description; ?>
</div>
<?php if($share_html){ ?>
  <div class="input-group-flex">
    <span><?= $text_share; ?></span>
    <div><?= $share_html; ?></div>
  </div>
<?php } ?>

<?php include_once('product_options.tpl'); ?>

<?= $waiting_module; ?>
<!-- 
<?php if ($review_status) { ?>
<div class="rating">
  <p>
    <?php for ($i = 1; $i <= 5; $i++) { ?>
    <?php if ($rating < $i) { ?>
    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
    <?php } else { ?>
    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
    <?php } ?>
    <?php } ?>
    <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?= $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?= $text_write; ?></a></p>
</div>
<?php } ?> -->