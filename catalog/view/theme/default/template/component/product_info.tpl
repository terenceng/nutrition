<div class="product-block <?= $out_of_stock; ?>">
	<div class="product-image-block relative image-zoom-hover">
		<?php if($sticker && $sticker['name']){ ?>
		<a 
		href="<?= $href; ?>" 
		title="<?= $name; ?>" 
		class="sticker absolute" 
		style="color: <?= $sticker['color']; ?>; background-color: <?= $sticker['background-color']; ?>">
			<?= $sticker['name']; ?>
		</a>
		<?php } ?>
		<div class="product-img">
			<a 
			href="<?= $href; ?>" 
			title="<?= $name; ?>" 
			class="product-image image-container relative" >

			<img 
				src="<?= $thumb; ?>" 
				alt="<?= $name; ?>" 
				title="<?= $name; ?>"
				class="img-responsive" />

			<?php if($more_options){ ?>
			<div class="more-options-text absolute position-bottom-center">
				<?= $more_options; ?>
			</div>
			<?php } ?>
			</a>
		</div>

		<div class="btn-group product-button">
<!--			<button
				type="button"
				<?php if($enquiry){ ?>
					onclick="enquiry.add('<?= $product_id; ?>', '<?= $minimum; ?>');"s
				<?php }else{ ?>
					onclick="cart.add('<?= $product_id; ?>', '<?= $minimum; ?>');"
				<?php } ?>
				class="btn btn-default">
				<i class="fa fa-shopping-cart"></i>
			</button>-->
                        <div><img onclick="window.location.href='<?= $href; ?>'" src="image/catalog/view.png" class="pointer"></div>
			<!--
			data-toggle="tooltip"
			<?php if($enquiry){ ?>
				title="<?= $button_enquiry; ?>"
				onclick="enquiry.add('<?= $product_id; ?>', '<?= $minimum; ?>');"s
			<?php }else{ ?>
				title="<?= $button_cart; ?>"
				onclick="cart.add('<?= $product_id; ?>', '<?= $minimum; ?>');"
			<?php } ?>
			-->
<!--			<button 
				type="button" 
				onclick="wishlist.add('<?= $product_id; ?>');" class="btn btn-default">
				<i class="fa fa-heart"></i>
			</button>-->
			<!--
			data-toggle="tooltip" 
			title="<?= $button_wishlist; ?>" 
			-->
<!--			<button 
				type="button" 
				
				onclick="compare.add('<?= $product_id; ?>');" class="btn btn-default">
				<i class="fa fa-exchange"></i>
			</button>-->
			<!--
			data-toggle="tooltip" 
			title="<?= $button_compare; ?>" 
			-->
		</div>
	</div>
	<div class="product-text">
		<?php if ($special_end && $special_end != '0000-00-00') { ?>
			<div id="" data-timer="<?= $special_end; ?>" class="clock-<?=$product_id;?> clock">
	
				<div class="days"></div>
				<div class="hours"></div>
				<div class="minutes"></div>
				<div class="seconds"></div>
				<script>
					var total_timer = $('.clock').size();
					
					for( var i=0; i<total_timer; i++ )
					{
						var timer = $('.clock-<?=$product_id;?>').attr('data-timer');
						// alert(timer);
						// console.log(i);
						$('.clock-<?=$product_id;?>').countdown(timer, function(event) {
						// alert(timer);
						// if(i>=total_timer)
						//   return false;
							// console.log(i+'test');
					
							// $(this).html(event.strftime('%D Day %H:%M:%S'));
							if(event.strftime('%D') > '1' || event.strftime('%D') > '01') {
								$(this).find(".days").html('<div class="timer">'+ event.strftime('%D') +'</div>Days');
								$(this).find(".hours").html('<div class="timer">'+ event.strftime('%H')+'</div>Hours');
								$(this).find(".minutes").html('<div class="timer">'+ event.strftime('%M')+'</div>Mins');
								$(this).find(".seconds").html('<div class="timer">'+ event.strftime('%S')+'</div>Secs');
							} else {
								$(this).find(".days").html('<div class="timer">'+ event.strftime('%D') +'</div>Day');
								$(this).find(".hours").html('<div class="timer">'+ event.strftime('%H')+'</div>Hours');
								$(this).find(".minutes").html('<div class="timer">'+ event.strftime('%M')+'</div>Mins');
								$(this).find(".seconds").html('<div class="timer">'+ event.strftime('%S')+'</div>Secs');
							}
					
						});
					}
				</script>
			</div> 
		<?php } ?>
		<div class="product-name">
			<a href="<?= $href; ?>"><?= $name; ?></a>
		</div>
	
		<div class="product-details">
			<?php if ($price && !$enquiry) { ?>
				<div class="price">
					<?php if (!$special) { ?>
						<?= $price; ?>
					<?php } else { ?>
						<span class="price-old"><?= $price; ?></span>
						<span class="price-new"><?= $special; ?></span>
					<?php } ?>
					<!-- <?php if ($tax) { ?>
						<span class="price-tax"><?= $text_tax; ?> <?= $tax; ?></span>
					<?php } ?> -->
				</div>
			<?php } ?>
	
			<?php /*if($enquiry){ ?>
			<span class="label label-primary">
				<?= $label_enquiry; ?>
			</span>
			<?php }*/ ?>
		</div>
			<button
					type="button"
							onclick="cart.add('<?= $product_id; ?>', '<?= $minimum; ?>');"
					class="btn btn-default"><?= $label_cart; ?>
			</button>
	</div>
	
</div>





