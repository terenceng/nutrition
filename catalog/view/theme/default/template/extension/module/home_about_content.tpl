<div class="home-about-content section">
	<div class="col-sm-6 col-left">
		<?php if($video) { ?>
			<div class="video">
				<?= html($video); ?>
			</div>
		<?php } else { ?>
			<div class="image">
				<img src="image/<?= $left_about_image ?>" class="img-responsive"/>
			</div>
		<?php } ?>

	</div>
	<div class="col-sm-6 col-right">
		<div class="header text-left">
			<h2>Nutrition Depot SG</h2>
			<div class="sub-heading bold-text">Itaque earum rerum</div>
			<br>
		</div>
		<div class="text">
			<?= html($right_about_text); ?>
		</div><br>
		<div class="more-button">
			<button class="btn btn-primary">Know More</button>
		</div>
	</div>
</div>