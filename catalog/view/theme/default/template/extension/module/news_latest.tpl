<!--<h3><?php echo strtoupper($heading_title); ?>
    <a style="text-decoration: none; font-size: 16px;" href="<?php echo $newslink; ?>"><?php echo $text_headlines; ?></a>
</h3>-->
  <div class="">
  	<?php if (!$article && isset($text_search_no_results)) { ?>
  		<h4><?php echo $text_search_no_results; ?></h4>
  	<?php } ?>
    <div id="news_latest" class="bnews-list owl-carousel">
		<?php foreach ($article as $articles) { ?>
			<div class="artblock">
				<?php if ($articles['thumb']) { ?>
                                    <div class='article_image'>
					<a href="<?php echo $articles['href']; ?>"><img class="article-image" align="left" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" /></a>
                                    </div>
				<?php } ?>
                                <div class='article_text'>
                                    <?php // if ($articles['date_added']) { ?>
                                        <!--<p class="article_date"><?php echo date("d F Y",strtotime($articles['date_added'])); ?></p>-->
                                    <?php // } ?>
                                    <?php if ($articles['name']) { ?>
                                            <div class="name skew_border_black_gray"><a href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a></div>
                                    <?php } ?>
                                    <?php if ($articles['custom1']) { ?>
                                            <div class="custom1"><?php echo $articles['custom1']; ?></div>
                                    <?php } ?>
                                    <?php if ($articles['description']) { ?>
                                            <div class="description"><?php echo $articles['description']; ?></div>
                                    <?php } ?>
                                    <?php if ($articles['button']) { ?>
                                            <div class="blog-button"><a class="button" href="<?php echo $articles['href']; ?>"><?php echo $button_more; ?></a></div>
                                    <?php } ?>
                                </div>
			</div>
		<?php } ?>
    </div>
  </div>
    
<script type="text/javascript"><!--
	$(document).ready(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
	});
//--></script>
<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>
<script type="text/javascript">
	// Note.. it supports Animate.css
	// Manual Slider don't support animate css
	$('#news_latest').owlCarousel({
			items: 1,
			<?php if (count($article) > 1) { ?>
				loop: false,
			<?php } ?>

			autoplay: true,
			autoplayTimeout: 5000,
			smartSpeed: 450,
			nav: true,
			navText: ['<div class="pointer absolute position-top-left h100 slider-nav slider-nav-left hover-show"></div>', '<div class="pointer absolute position-top-right h100 slider-nav slider-nav-right hover-show"></div>'],

			dots: true,
			dotsClass: 'slider-dots slider-custom-dots absolute position-bottom-left w100 list-inline text-center',

			//animateOut: 'slideOutDown',
			//animateIn: 'slideInDown',
    });
</script>