<div class="container extra-width">
	<div class="home-secondary-banner">
		<?php foreach($banners as $banner) { ?>
			<div class="secondary-banner">
				<a href="<?= $banner['image_link']; ?>">
					<img src="image/<?= $banner['image']; ?>" class="img-responsive"/>
				</a>
			</div>
		<?php }	?>
	</div>
</div>
	
	<script>
		$( document ).ready(function() {
			$(".home-secondary-banner").slick({
						dots:true,
						infinite: false,
						speed: 300,
						arrows:false,
						slidesToShow: 3,
						slidesToScroll: 1,
						responsive: [
							{
								breakpoint: 1199,
								settings: {
									slidesToShow: 3,
								}
							},
							{
								breakpoint: 991,
								settings: {
									slidesToShow: 2,
								}
							},
							{
								breakpoint: 767,
								settings: {
									slidesToShow: 1,
								}
							}
						],
						prevArrow: "<div class='pointer slick-nav left prev absolute'><i class='fa fa-angle-left fa-5x pmainimg-left'></i></div>",
						nextArrow: "<div class='pointer slick-nav right next absolute'><i class='fa fa-angle-right fa-5x pmainimg-right'></i></div>",
					});
		});
	</script>		
</div>