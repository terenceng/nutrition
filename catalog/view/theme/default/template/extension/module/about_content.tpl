<div class="about-content">
	<div class="about-top-section wrapper m-b-xl row">
			<div class="col-sm-7">
					<div class="title">
						<h2 class="text-align-left">
							<?= $about_top_main_title;?>
						</h2>
					</div>
					<div class="about-content-text">
						<?= html($about_top_content_text); ?>
					</div>
			</div>
	
			<div class="col-sm-5 about-top-image">
					<img class="img-responsive" src="image/<?= $about_top_image; ?>"/>
			</div>
	</div>
	
	<div class="about-section-2 wrapper m-b-xl row">
			<div class="col-sm-6 left">
					<div class="title">
						<h2 class="text-align-left">
							<?= $section2_left_title;?>
						</h2>
					</div>
					<div class="about-content-text">
						<?= html($section2_left_text); ?>
					</div>
			</div>
			<div class="col-sm-6 right">
					<div class="title">
						<h2 class="text-align-left">
							<?= $section2_right_title;?>
						</h2>
					</div>
					<div class="about-content-text">
						<?= html($section2_right_text); ?>
					</div>
			</div>
	</div>
	
	<div class="about-section-3 wrapper m-b-xl row">
			<div class="col-sm-12 text-center">
					<img class="img-responsive" src="image/<?= $section3_image; ?>"/>
			</div>
	
			<div class="col-sm-12">
					<div class="about-content-text">
						<?= html($section3_text); ?>
					</div>
			</div>
	
			<div class="col-sm-12 logos-wrapperflex">
					<?php foreach($logos as $logo) {	?>
						<div class="logo">
								<img class="img-responsive" src="image/<?= $logo['logo']; ?>"/>
						</div>
					<?php } ?>
			</div>
	</div>
	
	<div class="about-section-4 wrapper m-b-xl row">
			<div class="col-sm-6 left">
					<div class="title">
						<h2 class="text-align-left">
							<?= $section4_title;?>
						</h2>
					</div>
					<div class="about-content-text">
						<?= html($section4_text); ?>
					</div>
			</div>
			<div class="col-sm-6 right">
					<img class="img-responsive" src="image/<?= $section4_image; ?>"/>
			</div>
	</div>
	
	<div class="about-section-5 wrapper m-b-xl row">
			<div class="col-sm-6 left">
					<img class="img-responsive" src="image/<?= $section5_image; ?>"/>
			</div>
			<div class="col-sm-6 right">
					<div class="title">
						<h2 class="text-align-left">
							<?= $section5_title;?>
						</h2>
					</div>
					<div class="about-content-text">
						<?= html($section5_text); ?>
					</div>
			</div>
  </div>
	
	<script>
		$( document ).ready(function() {
			$(".logo-wrapper").slick({
						dots: false,
						infinite: false,
						speed: 300,
						slidesToShow: 5,
						slidesToScroll: 1,
						responsive: [
							{
								breakpoint: 1199,
								settings: {
									slidesToShow: 3,
								}
							},
							{
								breakpoint: 991,
								settings: {
									slidesToShow: 2,
								}
							},
							{
								breakpoint: 767,
								settings: {
									slidesToShow: 1,
								}
							}
						],
						prevArrow: "<div class='pointer slick-nav left prev absolute'><i class='fa fa-angle-left fa-5x pmainimg-left'></i></div>",
						nextArrow: "<div class='pointer slick-nav right next absolute'><i class='fa fa-angle-right fa-5x pmainimg-right'></i></div>",
					});
		});
	</script>		
</div>