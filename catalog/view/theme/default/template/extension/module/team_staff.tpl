<div class="content_team section max-offset">
    <div class="container extra-width">
        <div class="col-sm-12">
            <div class="col-sm-12 col-xs-12 showcase_main_slider">
                <?php foreach($teams as $team){ ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 single-staff m-b-xl">
                        <div class="team-image m-b-xl">
                            <img src="image/<?= $team['image']; ?>"  data-position="<?= $team["position"] ?>" data-title="<?= $team["title"] ?>" data-desc="<?= $team["description"] ?>" href="image/<?= $team['image']; ?>" alt="<?= $team['title']; ?>" class="test-class img-responsive">
                        </div>
                        <br>
                        <div class="position postion"><?= html_entity_decode($team['position']); ?></div>
                        <h4 class="team-title bold-text"><?= $team['title'] ?></h4>

                        <div class="description"><?= html_entity_decode($team['short_description']); ?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        var mp;
        $('.test-class').magnificPopup({
            type: 'image',
            mainClass: 'mfp-zoom-in mfp-img-mobile',
            removalDelay: 300,
            zoom: {
                enabled: true, // By default it's false, so don't forget to enable it

                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out', // CSS transition easing function
            },
            gallery: {enabled: true},
            callbacks: {
                beforeOpen: function() {
                    mp = $.magnificPopup.instance;
                },
                open: function() {
                    var t = $(mp.currItem.el[0]);

                    var image_link = t.attr("src");
                    console.log(image_link);
                    $(".mfp-position").html("<p>"+t.data('position')+"</p>");
                    $(".mfp-title").html(t.data('title'));
                    $(".mfp-description").html(t.data('desc'));
                    $(".staff-popup .image").css('background-image','url(' + image_link + ')');
                //overwrite default prev + next function. Add timeout for css3 crossfade animation
                $.magnificPopup.instance.next = function() {

                    var self = this;
                    self.wrap.removeClass('mfp-image-loaded');
                    setTimeout(function() { 
                        $.magnificPopup.proto.next.call(self); 
                        var t = $(mp.currItem.el[0]);
                        var image_link = t.attr("src");
                        $(".mfp-position").html("<p>"+t.data('position')+"</p>");
                        $(".mfp-title").html(t.data('title'));
                        $(".mfp-description").html(t.data('desc'));
                        $(".staff-popup .image").css('background-image','url(' + image_link + ')');
                    }, 120);
                };
                $.magnificPopup.instance.prev = function() {
                    var self = this;
                    self.wrap.removeClass('mfp-image-loaded');
                    setTimeout(function() { 
                        $.magnificPopup.proto.prev.call(self); 
                        var t = $(mp.currItem.el[0]);
                        var image_link = t.attr("src");
                        $(".mfp-position").html("<p>"+t.data('position')+"</p>");
                        $(".mfp-title").html(t.data('title'));
                        $(".mfp-description").html(t.data('desc'));
                        $(".staff-popup .image").css('background-image','url(' + image_link + ')');
                    }, 120);
                };
                },
                imageLoadComplete: function() {
                var self = this;
                setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
                }
                // beforeClose: function() {
            
            }
            // other options
        });
});
</script>