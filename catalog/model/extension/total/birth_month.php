<?php
class ModelExtensionTotalBirthMonth extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/birth_month');

		if(!$this->config->get('birth_month_status') || $this->config->get('birth_month_rate') < 0 || !$this->customer->isLogged()){
			return;
		}

		$customer_id = $this->customer->getId();

		$dob_used = 0;

		$usage = $this->config->get('birth_month_usage');

		$query = $this->db->query('SELECT DISTINCT customer_id
									FROM `' . DB_PREFIX . 'customer` 
									WHERE MONTH(dob) = MONTH(NOW())
									AND dob_used <'.$usage);
		if($query->num_rows == 0) return;

		$discount_total = 0;

		$rate = $this->config->get('birth_month_rate');
		
		$discount_total = $this->cart->getSubtotal() * ($rate/100);

		if ($discount_total > 0) {
			$total['totals'][] = array(
				'code'       => 'birth_month',
				'title'      => $this->language->get('text_birth_month'),
				'value'      => -$discount_total,
				'sort_order' => $this->config->get('birth_month_sort_order')
			);

			$total['total'] -= $discount_total;
		}

		$this->session->data['birth_month_applied'] = true;

	}
}
